# Use the official PHP image with Apache
FROM php:7.4-apache

# Install any necessary dependencies
RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

# Copy the application code to the container
COPY . /var/www/html/

# Enable mod_rewrite for Apache
RUN a2enmod rewrite

# Change the ownership of the application files to www-data (Apache user)
RUN chown -R www-data:www-data /var/www/html

# Expose port 80 for HTTP and 443 for HTTPS
EXPOSE 80 443

# Command to run Apache in the foreground
CMD ["apache2-foreground"]
